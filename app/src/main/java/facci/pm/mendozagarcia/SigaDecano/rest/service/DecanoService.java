package facci.pm.mendozagarcia.SigaDecano.rest.service;

import java.util.List;

import facci.pm.mendozagarcia.SigaDecano.rest.constanst.ApiConstanst;
import facci.pm.mendozagarcia.SigaDecano.rest.modelo.Decano;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DecanoService {

    @GET(ApiConstanst.ADMINISTRATIVOS)
    Call<List<Decano>> getAdministrativos();

    @GET(ApiConstanst.ADMINISTRATIVO)
    Call<DecanoService> getAdministrativo(@Path ("id") String id);







}
