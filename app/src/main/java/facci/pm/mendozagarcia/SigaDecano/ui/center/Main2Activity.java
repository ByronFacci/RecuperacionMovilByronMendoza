package facci.pm.mendozagarcia.SigaDecano.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import facci.pm.mendozagarcia.SigaDecano.R;
import facci.pm.mendozagarcia.SigaDecano.rest.adapter.SigaAdapter;
import facci.pm.mendozagarcia.SigaDecano.rest.modelo.Decano;
import facci.pm.mendozagarcia.SigaDecano.rest.service.DecanoService;
import retrofit2.Call;

public class Main2Activity extends AppCompatActivity {

    TextView id2, nombre2, apellido2, cargo2, nombramiento2 ;

    ImageView Img2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String id = getIntent().getStringExtra("id");

        id2 = (TextView)findViewById(R.id.IdAd2);
        nombre2 = (TextView)findViewById(R.id.TxtNombre2);
        apellido2 = (TextView)findViewById(R.id.TxtApellido2);
        cargo2 = (TextView)findViewById(R.id.TxtCargo2);
        nombramiento2 = (TextView)findViewById(R.id.TxtNombramiento2);

        Img2 = (ImageView)findViewById(R.id.ImgAd2);

        Estmostrar2(id);
    }

    private void Estmostrar2(String id) {
        SigaAdapter sigaAdapter = new SigaAdapter();
        Call<DecanoService> decanoCallCall = sigaAdapter.getAdministrativo(id);
        decanoCallCall
        @Override
            public void onResponse ((Call < Decano) > call, Response<Decano> response) {


                Decano administrativos = response.body();
                Log.e("DECANO", administrativos.getNombre());

                nombre2.setText("NOMBRES: " + administrativos.getNombre().toString());
                apellido2.setText("APELLIDOS: " + administrativos.getApellido().toString());
                cargo2.setText("PARCIAL 1: " + administrativos.getCargo().toString());
                nombramiento2.setText("PARCIAL 2: " + administrativos.getNombramiento().toString());

                Picasso.get().load(administrativos.getFoto()).into(Img2);

            }

            @Override
            public void onFailure(Call<Decano> call, Throwable ) {

            }
        });

    }
}
