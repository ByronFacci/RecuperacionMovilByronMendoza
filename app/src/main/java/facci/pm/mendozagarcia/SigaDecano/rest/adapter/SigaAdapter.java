package facci.pm.mendozagarcia.SigaDecano.rest.adapter;

import java.util.List;

import facci.pm.mendozagarcia.SigaDecano.rest.constanst.ApiConstanst;
import facci.pm.mendozagarcia.SigaDecano.rest.modelo.Decano;
import facci.pm.mendozagarcia.SigaDecano.rest.service.DecanoService;
import retrofit2.Call;

public class SigaAdapter extends BaseAdapter implements DecanoService{

    private DecanoService decanoService;

    public SigaAdapter (){
        super(ApiConstanst.BASE_ADMINISTRATIVOS_URL);

        decanoService = createService(DecanoService.class);

    }

    @Override
    public Call<List<Decano>> getAdministrativos() { return decanoService.getAdministrativos(); }

    @Override
    public Call<DecanoService> getAdministrativo (String id) { return decanoService.getAdministrativo(id);}


}
