package facci.pm.mendozagarcia.SigaDecano.rest.constanst;

public class ApiConstanst {

    public static final String BASE_ADMINISTRATIVOS_URL = "http://159.65.168.44:3003/administrativos/";

    public static final String ADMINISTRATIVOS = "administrativos";

    public static final String ADMINISTRATIVO = "administrativo/{id}";
}
