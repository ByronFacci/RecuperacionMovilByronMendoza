package facci.pm.mendozagarcia.SigaDecano.rest.modelo;

import com.google.gson.annotations.SerializedName;

public class Decano {

    @SerializedName("id")
    private String id;

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("apellido")
    private String apellido;

    @SerializedName("cargo")
    private String cargo;

    @SerializedName("nombramiento")
    private String nombramiento;

    @SerializedName("foto")
    private String foto;


    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getCargo() {
        return cargo;
    }

    public String getNombramiento() {
        return nombramiento;
    }

    public String getFoto() {
        return foto;
    }
}
